from flask import g, current_app

from ..helpers.samsadhani import Samsadhani, SamsadhaniInterface

def push_environ_to_g():
    samsadhani = Samsadhani(current_app.config['SAMSADHANI_API_ROOT'])
    samsadhani_interface = SamsadhaniInterface(samsadhani)

    g.samsadhani = samsadhani
    g.samsadhani_interface = samsadhani_interface
