
import flask
import flask_restplus
from flask import g
from requests import HTTPError

from vedavaapi.client import VedavaapiSession, objstore

from ..helpers.samsadhani import SamsadhaniInterface
from . import api


# noinspection PyMethodMayBeStatic
@api.route('/')
class RootDocs(flask_restplus.Resource):

    def get(self):
        return flask.redirect('docs')


def error_response(message, http_error):
    try:
        resp_json = http_error.response.json()
    except:
        resp_json = {}

    return {
        'message': message,
        'error': {
            'response': resp_json,
            'status_code': http_error.response.status_code
        }
    }, http_error.response.status_code


@api.route('/morph')
class MorphAnalyzer(flask_restplus.Resource):

    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)
    post_parser.add_argument('target', location='form', required=True)
    post_parser.add_argument('return_projection', location='form')

    get_parser = api.parser()
    get_parser.add_argument('text', location='args', required=True)

    @api.expect(get_parser, validate=True)
    def get(self):
        args = self.get_parser.parse_args()
        text = args['text']
        isamsadhani = g.samsadhani_interface  # type: SamsadhaniInterface
        try:
            graph, choice_anno_item_gids_list = isamsadhani.morph_phrases('_:target', text)
        except HTTPError as e:
            return error_response('error in invoking samsadhani', e)
        phrase_analysis_list = [
            {
                "choice_anno": graph[choice_anno_item_gids['choice_anno']],
                "choice_items": [graph[item_gid] for item_gid in choice_anno_item_gids['choice_items']]
            }
            for choice_anno_item_gids in choice_anno_item_gids_list
        ]
        return {
            "analysis": "Morph",
            "text": text,
            "result": phrase_analysis_list
        }

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        vc = VedavaapiSession(args['site_url'])
        vc.set_access_token(args['access_token'])

        try:
            resource = objstore.get_resource(vc, args['target'])
        except HTTPError as e:
            return error_response('error in getting resource', e)

        if not resource['resolvedPermissions']['createAnnos']:
            return {'message': 'no permission to create annotations over given resource'}, 403

        target_text = resource['content'] if isinstance(resource.get('content', None), str) else None

        our_response = {
            "analysis": "Morph",
            "target": args["target"],
            "text": target_text,
        }

        if target_text is None:
            our_response['result'] = []
            return our_response

        isamsadhani = g.samsadhani_interface  # type: SamsadhaniInterface
        try:
            graph, choice_anno_item_gids_list = isamsadhani.morph_phrases(args['target'], target_text)
        except HTTPError as e:
            return error_response('error in invoking samsadhani', e)

        try:
            response = objstore.post_graph(
                vc, graph, response_projection_map=None, should_return_oold_resources=True, upsert=True)
        except HTTPError as e:
            return error_response('error in saving morph annotations', e)

        phrase_analysis_list = [
            {
                "choice_anno": response['graph'][choice_anno_item_gids['choice_anno']],
                "choice_items": [response['graph'][item_gid] for item_gid in choice_anno_item_gids['choice_items']]
            }
            for choice_anno_item_gids in choice_anno_item_gids_list
        ]
        our_response['result'] = phrase_analysis_list
        return our_response
