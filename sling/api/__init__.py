import flask_restplus
from flask import Blueprint, g
from . import environ

api_blueprint_v1 = Blueprint('sling' + '_v1', __name__)

api = flask_restplus.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Vedavaapi Sling API',
    doc='/docs',
    description='''
    A wrapper api around Samsadhani
    '''
)

api_blueprint_v1.before_request(environ.push_environ_to_g)

from . import rest
