import os

import flask
from flask_cors import CORS
import logging

app = flask.Flask(__name__, instance_relative_config=True)
CORS(app)

try:
    app.config.from_json(filename="config.json")
except FileNotFoundError as e:
    pass

samsadhani_api_root = app.config.get('SAMSADHANI_API_ROOT', os.environ.get('SAMSADHANI_API_ROOT'))

if not samsadhani_api_root:
    logging.warning(
        'SAMSADHANI_API_ROOT is not found in environment or instance config file. falling back on http://localhost/cgi-bin/scl')
    samsadhani_api_root = 'http://localhost/cgi-bin/scl'

app.config['SAMSADHANI_API_ROOT'] = samsadhani_api_root

from .api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='')
