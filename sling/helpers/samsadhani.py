import os
import re

import requests

from sling.helpers.selection_helper import get_phrase_selectors


class Samsadhani(object):

    def __init__(self, api_root):
        self.api_root = api_root.rstrip('/') + '/'

    def morph_url(self):
        return os.path.join(self.api_root, 'morph/morph.cgi')

    def morph(self, morfword, encoding='Unicode', json_out=True):
        url = self.morph_url()
        print('invoking Samsadhani Morph at {}'.format(url))

        data = {
            'morfword': morfword,
            'encoding': encoding,
            'json_out': json_out
        }

        response = requests.post(url, data=data)
        response.raise_for_status()
        return response.json()


class SamsadhaniInterface(object):

    VALID_INPUT_RE = re.compile(r'[अआइईउऊऋॠऌएऐओऔअंअःकखगघङचछजझञटठडढणतथदधनपफबभमयरलवशषसह ्ािीुूृॄॢेैोौंः\-]+')

    def __init__(self, samsadhani: Samsadhani):
        self.samsadhani = samsadhani

    def morph(self, target, selector, _id=None):
        phrase = selector['refinedBy']['exact']
        response = self.samsadhani.morph(phrase)
        results = response.get('result', [])  # type: list

        graph = {}
        choice_anno_gid = _id or '_:{}-{}-{}'.format(target, selector['start'], selector['end'])

        choice_anno = {
            "_id": choice_anno_gid,
            "jsonClass": "ChoiceAnnotation",
            "generator": "Samsadhani",
            "target": target,
            "targetSelector": selector,
            "membersClass": "MorphAnnotation",
            "choices": {},
            "jsonClassLabel": "Choice:Morph:{}".format(phrase)
        }
        graph[choice_anno_gid] = choice_anno

        choice_item_gids = []

        for i, result in enumerate(results):
            choice_item_blank_id = '_:{}-{}'.format(choice_anno_gid, i)
            morph_anno = {
                "jsonClass": "MorphAnnotation",
                "generator": "Samsadhani",
                "target": choice_anno_gid,
                "body": result
            }
            graph[choice_item_blank_id] = morph_anno
            choice_item_gids.append(choice_item_blank_id)

        return graph, {
            "choice_anno": choice_anno_gid,
            "choice_items": choice_item_gids
        }

    def is_valid_input(self, input):
        return bool(self.VALID_INPUT_RE.fullmatch(input))

    def morph_phrases(self, target, target_text):
        target_phrase_selectors = get_phrase_selectors(target_text)
        graph = {}

        choice_anno_item_gids_list = []

        for i, phrase_selector in enumerate(target_phrase_selectors):
            phrase = phrase_selector['refinedBy']['exact']
            if not self.is_valid_input(phrase):
                continue
            phrase_morph_graph, choice_anno_item_gids = self.morph(
                target, phrase_selector, _id='_:{}-{}'.format(target, i))
            graph.update(phrase_morph_graph)
            choice_anno_item_gids_list.append(choice_anno_item_gids)

        return graph, choice_anno_item_gids_list
