import re
import string

phrase_token_re = re.compile(r'([^\s।॥{punct}]*)([\s।॥{punct}]*)'.format(punct=re.escape(string.punctuation)))

def get_phrase_selectors(text):
    token_groups = phrase_token_re.findall(text)

    prev_end = 0
    selectors = []

    for i, tg in enumerate(token_groups):
        start = prev_end
        phrase = tg[0]
        end = prev_end + len(phrase)
        prev_end = end + len(tg[1])

        if not phrase:
            continue
        # TODO set prefix and suffix too properly

        selector = {
            "jsonClass": "TextPositionSelector",
            "start": start,
            "end": end,
            "refinedBy": {
                "jsonClass": "TextQuoteSelector",
                "exact": phrase
            }
        }
        selectors.append(selector)

    return selectors
